FROM python:3
WORKDIR /code
COPY ./django_blog/requirements.txt /code/
RUN pip install -r requirements.txt
COPY ./django_blog/ /code/
